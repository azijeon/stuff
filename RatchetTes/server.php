<?php
header( 'Content-type: text/html; charset=utf-8' );

include "boot.php";

use Ratchet\ConnectionInterface as ConnectionInterface;
use Tracy\Debugger;

class MyApp implements \Ratchet\MessageComponentInterface
{
	protected $clients;

	protected $stop_server;
	
    public function __construct($stop_callback) 
	{
        $this->clients		= new \SplObjectStorage;
		$this->stop_server	= $stop_callback;
    }

    public function onOpen(ConnectionInterface $conn) 
	{
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        $this->log("New connection! ({$conn->resourceId})");
    }

    public function onMessage(ConnectionInterface $from, $msg) 
	{
        $numRecv = count($this->clients) - 1;
		
        $this->log(sprintf('Connection %d sending message "%s" to %d other connection%s'
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's'));

		if(trim($msg) == "stop")
			$this->stop();
		
        foreach ($this->clients as $client)
		{
            if ($from !== $client)
			{
                // The sender is not the receiver, send to each client connected
                $client->send("User {$from->resourceId}: $msg");
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
	{
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

       $this->log("Connection {$conn->resourceId} has disconnected");
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
	{
        $this->log("An error has occurred: {$e->getMessage()}");

        $conn->close();
    }
	
	public function stop()
	{
		foreach($this->clients as $client)
		{
			$client->send("Bye!");
		}
		
		exit;
	}
	
	protected function log($message)
	{
		echo $message, '<br/>';
		
		flush();
		ob_flush();
	}
}


\ob_implicit_flush(true);

$port = isset($_GET['port']) ? (int) $_GET['port'] : 823;

$server = \Ratchet\Server\IoServer::factory(
		new \Ratchet\Http\HttpServer(
			new \Ratchet\WebSocket\WsServer(
					
				new MyApp('stop_server')
			)			
		), 
		$port);
				
$pid = getmypid();

echo "<p>My pid is: {$pid}</p>";
flush();
ob_flush();

$server->run();