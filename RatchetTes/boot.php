<?php
include __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function($class)
{
	$path = __DIR__ . '/Classes/' . str_replace('\\', '/', $class) .'.php';
	
	if(file_exists($path))
	{
		include $path;
	}
});
