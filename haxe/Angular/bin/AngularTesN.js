(function () { "use strict";
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Main = function() { };
Main.__name__ = ["Main"];
Main.main = function() {
	var app = new module.TestApp();
	return;
};
var IMap = function() { };
IMap.__name__ = ["IMap"];
var Reflect = function() { };
Reflect.__name__ = ["Reflect"];
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		return null;
	}
};
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
};
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
};
Reflect.isObject = function(v) {
	if(v == null) return false;
	var t = typeof(v);
	return t == "string" || t == "object" && v.__enum__ == null || t == "function" && (v.__name__ || v.__ename__) != null;
};
Reflect.makeVarArgs = function(f) {
	return function() {
		var a = Array.prototype.slice.call(arguments);
		return f(a);
	};
};
var Type = function() { };
Type.__name__ = ["Type"];
Type.getClass = function(o) {
	if(o == null) return null;
	if((o instanceof Array) && o.__enum__ == null) return Array; else return o.__class__;
};
Type.getClassName = function(c) {
	var a = c.__name__;
	return a.join(".");
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw "Too many arguments";
	}
	return null;
};
var azijeon = {};
azijeon.angular = {};
azijeon.angular.Injectable = function(args) {
	this.injected = args;
};
azijeon.angular.Injectable.__name__ = ["azijeon","angular","Injectable"];
azijeon.angular.Injectable.prototype = {
	__class__: azijeon.angular.Injectable
};
azijeon.angular.Controller = function(args) {
	azijeon.angular.Injectable.call(this,args);
};
azijeon.angular.Controller.__name__ = ["azijeon","angular","Controller"];
azijeon.angular.Controller.__super__ = azijeon.angular.Injectable;
azijeon.angular.Controller.prototype = $extend(azijeon.angular.Injectable.prototype,{
	__class__: azijeon.angular.Controller
});
azijeon.angular.Directive = function() {
	this.count = 0;
	this.scope = null;
	this.templateUrl = null;
	this.template = null;
	this.restirct = "E";
};
azijeon.angular.Directive.__name__ = ["azijeon","angular","Directive"];
azijeon.angular.Directive.prototype = {
	__class__: azijeon.angular.Directive
};
azijeon.angular.Module = function(name,dependencies) {
	if(name == null) name = Type.getClassName(Type.getClass(this)).split(".").pop();
	this.app_name = name;
	if(dependencies == null) dependencies = [];
	this.dependencies = dependencies;
	this.ng_module = window.angular.module(this.app_name,dependencies);
};
azijeon.angular.Module.__name__ = ["azijeon","angular","Module"];
azijeon.angular.Module.prototype = {
	controller: function(name,ctrl) {
		this.ng_module.controller(name,azijeon.angular.NgHelper.prepareDefinition(ctrl));
		return this;
	}
	,factory: function(name,cls) {
		this.ng_module.factory(name,azijeon.angular.NgHelper.prepareDefinition(cls));
		return this;
	}
	,directive: function(name,def) {
		this.ng_module.directive(name,function() {
			var inst = Type.createInstance(def,[]);
			if(Reflect.isFunction(Reflect.field(inst,"controller"))) {
				var inject = [];
				var meta = haxe.rtti.Meta.getFields(def);
				if(meta.controller && meta.controller.inject) inject = meta.controller.inject;
				var controller = Reflect.field(inst,"controller");
				inject.push(Reflect.makeVarArgs(function(args) {
					controller.apply(inst,args);
				}));
				inst.controller = inject;
			}
			return inst;
		});
		return this;
	}
	,config: function(cb) {
		this.ng_module.config(cb);
		return this;
	}
	,__class__: azijeon.angular.Module
};
azijeon.angular.NgHelper = function() { };
azijeon.angular.NgHelper.__name__ = ["azijeon","angular","NgHelper"];
azijeon.angular.NgHelper.getInjections = function(cls) {
	var inject = haxe.rtti.Meta.getType(cls).inject;
	if((inject instanceof Array) && inject.__enum__ == null) return inject; else return [];
};
azijeon.angular.NgHelper.valToArray = function(val) {
	if((val instanceof Array) && val.__enum__ == null) return val;
	if(val != null) return [val];
	return [];
};
azijeon.angular.NgHelper.prepareDefinition = function(ctrl) {
	var def = azijeon.angular.NgHelper.getInjections(ctrl);
	var ctrl_factory = Reflect.makeVarArgs(function(args) {
		var input = new haxe.ds.StringMap();
		var i = 0;
		var _g = 0;
		while(_g < def.length) {
			var key = def[_g];
			++_g;
			input.set(key,args[i++]);
		}
		var instance = Type.createInstance(ctrl,[input]);
		return instance;
	});
	def.push(ctrl_factory);
	return def;
};
azijeon.angular._NgScope = {};
azijeon.angular._NgScope.NgScope_Impl_ = function() { };
azijeon.angular._NgScope.NgScope_Impl_.__name__ = ["azijeon","angular","_NgScope","NgScope_Impl_"];
azijeon.angular._NgScope.NgScope_Impl_._new = function(i) {
	return i;
};
azijeon.angular._NgScope.NgScope_Impl_.evalAsync = function(this1,cb) {
	Reflect.callMethod(this1,Reflect.field(this1,"$evalAsync"),[cb]);
};
azijeon.angular._NgScope.NgScope_Impl_["eval"] = function(this1,cb) {
	Reflect.callMethod(this1,Reflect.field(this1,"$eval"),[cb]);
};
azijeon.angular._NgScope.NgScope_Impl_.emit = function(this1,event_name,args) {
	Reflect.callMethod(this1,Reflect.field(this1,"$emit"),[event_name,args]);
};
azijeon.angular._NgScope.NgScope_Impl_.broadcast = function(this1,event_name,args) {
	Reflect.callMethod(this1,Reflect.field(this1,"$broadcast"),[event_name,args]);
};
azijeon.angular._NgScope.NgScope_Impl_.get = function(this1,name) {
	var path = name.split(".");
	var node = this1;
	while(path.length > 0) {
		var key = path.shift();
		if(Object.prototype.hasOwnProperty.call(node,key)) node = Reflect.field(node,key); else return null;
	}
	return node;
};
azijeon.angular._NgScope.NgScope_Impl_.set = function(this1,name,value) {
	var path = name.split(".");
	var node = this1;
	while(path.length > 1) {
		var key = path.shift();
		if(!Object.prototype.hasOwnProperty.call(node,key) || !Reflect.isObject(Reflect.field(node,key))) node[key] = { };
		node = Reflect.field(node,key);
	}
	var key1 = path.shift();
	node[key1] = value;
};
azijeon.angular.Service = function(args) {
	azijeon.angular.Injectable.call(this,args);
};
azijeon.angular.Service.__name__ = ["azijeon","angular","Service"];
azijeon.angular.Service.__super__ = azijeon.angular.Injectable;
azijeon.angular.Service.prototype = $extend(azijeon.angular.Injectable.prototype,{
	__class__: azijeon.angular.Service
});
var controller = {};
controller.Route1Ctrl = function(args) {
	azijeon.angular.Controller.call(this,args);
	var scope = this.injected.get("$scope");
	var params = this.injected.get("$routeParams");
	console.log("Route Ctrl",params);
	azijeon.angular._NgScope.NgScope_Impl_.set(scope,"some_id",params.some_id);
};
controller.Route1Ctrl.__name__ = ["controller","Route1Ctrl"];
controller.Route1Ctrl.__super__ = azijeon.angular.Controller;
controller.Route1Ctrl.prototype = $extend(azijeon.angular.Controller.prototype,{
	__class__: controller.Route1Ctrl
});
controller.TestCtrl = function(args) {
	azijeon.angular.Controller.call(this,args);
	console.log("TestCtrl : ",this.injected.get("$scope"),this.injected);
};
controller.TestCtrl.__name__ = ["controller","TestCtrl"];
controller.TestCtrl.__super__ = azijeon.angular.Controller;
controller.TestCtrl.prototype = $extend(azijeon.angular.Controller.prototype,{
	__class__: controller.TestCtrl
});
var directive = {};
directive.TestDir = function() {
	azijeon.angular.Directive.call(this);
	this.templateUrl = "template/test.html";
	this.scope = { };
};
directive.TestDir.__name__ = ["directive","TestDir"];
directive.TestDir.__super__ = azijeon.angular.Directive;
directive.TestDir.prototype = $extend(azijeon.angular.Directive.prototype,{
	controller: function(scope) {
		console.log("Dir Controller",scope);
		azijeon.angular._NgScope.NgScope_Impl_.set(scope,"count",0);
		azijeon.angular._NgScope.NgScope_Impl_.set(scope,"clickIt",function(e) {
			console.log("U kliqd it",e);
			azijeon.angular._NgScope.NgScope_Impl_.set(scope,"count",azijeon.angular._NgScope.NgScope_Impl_.get(scope,"count") + 1);
		});
	}
	,__class__: directive.TestDir
});
var haxe = {};
haxe.ds = {};
haxe.ds.StringMap = function() {
	this.h = { };
};
haxe.ds.StringMap.__name__ = ["haxe","ds","StringMap"];
haxe.ds.StringMap.__interfaces__ = [IMap];
haxe.ds.StringMap.prototype = {
	set: function(key,value) {
		this.h["$" + key] = value;
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,__class__: haxe.ds.StringMap
};
haxe.rtti = {};
haxe.rtti.Meta = function() { };
haxe.rtti.Meta.__name__ = ["haxe","rtti","Meta"];
haxe.rtti.Meta.getType = function(t) {
	var meta = t.__meta__;
	if(meta == null || meta.obj == null) return { }; else return meta.obj;
};
haxe.rtti.Meta.getFields = function(t) {
	var meta = t.__meta__;
	if(meta == null || meta.fields == null) return { }; else return meta.fields;
};
var js = {};
var module = {};
module.TestApp = function() {
	azijeon.angular.Module.call(this,null,["ngRoute"]);
	this.controller("TestCtrl",controller.TestCtrl);
	this.controller("Route1Ctrl",controller.Route1Ctrl);
	this.directive("test",directive.TestDir);
	this.config(["$routeProvider",function(routeProvider) {
		console.log("Config: ",routeProvider);
		routeProvider.when("/Test",{ templateUrl : "template/route1.html", controller : "Route1Ctrl"}).when("/Test/:some_id/",{ templateUrl : "template/route1.html", controller : "Route1Ctrl"}).otherwise({ templateUrl : "template/default.html", controller : "TestCtrl"});
	}]);
};
module.TestApp.__name__ = ["module","TestApp"];
module.TestApp.__super__ = azijeon.angular.Module;
module.TestApp.prototype = $extend(azijeon.angular.Module.prototype,{
	__class__: module.TestApp
});
String.prototype.__class__ = String;
String.__name__ = ["String"];
Array.__name__ = ["Array"];
var q = window.jQuery;
js.JQuery = q;
controller.Route1Ctrl.__meta__ = { obj : { inject : ["$scope","$routeParams"]}};
controller.TestCtrl.__meta__ = { obj : { inject : ["$scope","$http","$route"]}};
directive.TestDir.__meta__ = { fields : { controller : { inject : ["$scope"]}}};
Main.main();
})();
