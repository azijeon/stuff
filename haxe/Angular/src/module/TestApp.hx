package module;

import azijeon.angular.Module;
import controller.*;
import directive.*;

/**
 * ...
 * @author Az
 */
class TestApp extends Module
{
	public function new()
	{
		super(null, ["ngRoute"]);
		
		controller("TestCtrl", 		TestCtrl);
		controller("Route1Ctrl", 	Route1Ctrl);
		directive("test", 		TestDir);
		
		config(["$routeProvider", function(routeProvider)
		{
			untyped console.log("Config: ", routeProvider);
			
			// A test route
			routeProvider.when("/Test", 
			{
				templateUrl : "template/route1.html",
				controller 	: "Route1Ctrl",				
			})
			
			// A test route with param
			.when("/Test/:some_id/", 
			{
				templateUrl : "template/route1.html",
				controller 	: "Route1Ctrl",
			})
			
			.otherwise(
			{
				templateUrl : "template/default.html",
				controller  : "TestCtrl",
			});			
			
		}]);
	}
}
