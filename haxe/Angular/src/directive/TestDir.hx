package directive;

import angular.*;
import azijeon.angular.Directive;
import azijeon.angular.NgScope;

/**
 * ...
 * @author Az
 */
class TestDir extends Directive
{
	public function new()
	{
		super();
		templateUrl = "template/test.html";
		
		scope = { };
	}
	
	@inject("$scope")
	public function controller(scope:NgScope):Void
	{
		untyped console.log("Dir Controller", scope);
		
		scope["count"] = 0;
		
		scope["clickIt"] = function(e)
		{
			untyped console.log("U kliqd it", e);
			scope["count"] += 1;
		}
	}
}