package controller;

import azijeon.angular.Controller;
import azijeon.angular.NgScope;
import haxe.ds.StringMap;

/**
 * ...
 * @author Az
 */
@inject("$scope", "$routeParams")
class Route1Ctrl extends Controller
{
	
	public function new(args:StringMap<Dynamic>)
	{
		super(args);
		
		var scope : NgScope	= injected.get("$scope");
		var params 			= injected.get("$routeParams");
		
		untyped console.log("Route Ctrl", params);
		
		scope["some_id"] = params.some_id;		
	}
	
}