package controller;
import azijeon.angular.Controller;
import haxe.ds.StringMap;

/**
 * ...
 * @author Az
 */
@inject("$scope", "$http", "$route")
class TestCtrl extends Controller
{
	public function new(args:StringMap<Dynamic>) 
	{
		super(args);
		
		untyped console.log("TestCtrl : ", injected.get("$scope"), injected);
	}
}