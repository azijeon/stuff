package azijeon.angular;
import haxe.rtti.Meta;
import haxe.ds.StringMap;

/**
 * ...
 * @author Az
 */
class NgHelper
{
	public static function getInjections(cls:Class<Dynamic>):Array<String>
	{
		var inject = Meta.getType(cls).inject;
		
		if (Std.is(inject, Array))
			return cast inject;
		
		else
			return [];
	}
	
	public static inline function valToArray(val:Dynamic):Dynamic
	{
		if (Std.is(val, Array))
			return val;
			
		if (val != null)
			return [val];
			
		return [];
	}
	
	public static function prepareDefinition(ctrl:Class<Injectable>):Array<Dynamic>
	{
		var def = NgHelper.getInjections(ctrl);
		
		var ctrl_factory = Reflect.makeVarArgs(function(args:Array<Dynamic>)
		{
			var input : StringMap<Dynamic> = new StringMap();
			
			var i : Int = 0;
			
			for (key in def)
			{
				if (Std.is(key, String))
				{
					input.set(key, args[i++]);
				}
			}
			
			var instance = Type.createInstance(ctrl, [input]);
			
			return instance;
		});
		
		// Add ctrl factory to definition
		def.push(ctrl_factory);
		
		return def;
		
	}
	
}