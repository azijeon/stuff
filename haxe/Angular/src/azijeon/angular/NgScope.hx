package azijeon.angular;
import js.Lib;

/**
 * ...
 * @author Az
 */
abstract NgScope(Dynamic) from Dynamic to Dynamic
{
	inline public function new(i:Dynamic) { this = i; }
		
	public function evalAsync(?cb:Dynamic):Void
	{
		Reflect.callMethod(this, Reflect.field(this, "$evalAsync"), [cb]);
	}
	
	public function eval(?cb:Dynamic):Void
	{
		Reflect.callMethod(this, Reflect.field(this, "$eval"), [cb]);
	}
	
	public function emit(event_name:String, ?args:Array<Dynamic>):Void
	{
		Reflect.callMethod(this, Reflect.field(this, "$emit"), [event_name, args]);
	}
	
	public function broadcast(event_name:String, ?args:Array<Dynamic>):Void
	{
		Reflect.callMethod(this, Reflect.field(this, "$broadcast"), [event_name, args]);
	}
	
	public function watch(expr:Dynamic, listener:Dynamic, ?euquality:Dynamic):Void
	{
		Reflect.callMethod(this, Reflect.field(this, "$watch"), untyped __js__("Array.prototype.slice.call(arguments, 1)"));
	}
	
	@:arrayAccess
	public function get(name:String):Dynamic
	{
		var path : Array<String> = name.split(".");
		
		var node : Dynamic = this;
		
		while (path.length > 0)
		{
			var key: String = path.shift();
			
			if (Reflect.hasField(node, key))
			{
				node = Reflect.field(node, key);
			}
			else
			{
				return null;
			}
			
		}
		
		return node;
	}
	
	@:arrayAccess
	public function set(name:String, value:Dynamic):Void
	{
		var path : Array<String> 	= name.split(".");
		var node : Dynamic 			= this;
		
		while (path.length > 1)
		{
			var key : String = path.shift();
			
			if (!Reflect.hasField(node, key) || !Reflect.isObject(Reflect.field(node, key)))
			{
				Reflect.setField(node, key, {});
			}
			
			node = Reflect.field(node, key);
		}
		
		var key : String = path.shift();
		Reflect.setField(node, key, value);
	}
}
