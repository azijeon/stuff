package azijeon.angular;

import azijeon.angular.Angular.NgModule;
import haxe.ds.StringMap;
import js.html.Element;
import haxe.rtti.Meta;

/**
 * ...
 * @author Az
 */
class Module
{
	private var app_name : String;
	private var dependencies : Array<String>;
	
	private var ng_module : NgModule;
	
	public function new(?name:String, ?dependencies:Array<String>) 
	{
		if (name == null)
		{
			name = Type.getClassName(Type.getClass(this)).split(".").pop();
		}
		
		app_name = name;
		
		if (dependencies == null)
		{
			dependencies = [];
		}
		
		this.dependencies = dependencies;
		
		ng_module = Angular.module(app_name, dependencies);
	}
	
		
	/**
	 * Register a controller
	 * 
	 * @param	name
	 * @param	ctrl
	 * @return
	 */
	public function controller(name:String, ctrl:Class<Controller>):Module
	{
		ng_module.controller(name, NgHelper.prepareDefinition(cast ctrl));
		return this;
	}
	
	public function factory(name:String, cls:Class<Service>):Module
	{
		ng_module.factory(name, NgHelper.prepareDefinition(cast cls));
		return this;
	}
	
	/**
	 * Register a directive
	 * 
	 * @param	name
	 * @param	def
	 * @return
	 */
	public function directive(name:String, def:Class<Directive>):Module
	{
		ng_module.directive(name, function()
		{
			var inst = Type.createInstance(def, []);
			
			if(Reflect.isFunction(Reflect.field(inst, "controller")))
			{
				var inject = [];
				
				var meta : Dynamic = Meta.getFields(def);
				
				if (meta.controller && meta.controller.inject)
				{
				
					inject = meta.controller.inject;
				}
				
				var controller = Reflect.field(inst, "controller");
				
				inject.push(Reflect.makeVarArgs(function(args:Array<Dynamic>){
					
					Reflect.callMethod(inst, controller, args);
					
				}));
				
				Reflect.setField(inst, "controller", inject);
				
			}
			
			return inst;
		});
		return this;
	}
	
	public function config(cb:Dynamic):Module
	{
		ng_module.config(cb);
		
		return this;
	}
}