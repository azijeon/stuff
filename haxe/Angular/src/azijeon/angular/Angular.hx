package azijeon.angular;

import js.html.Element;
import haxe.macro.Compiler;

/**
 * ...
 * @author Az
 */
@:native("window.angular")
extern class Angular
{
	public static function __init__():Void
	{
		#if macro
		
		#end
	}
	
	public static function module(name:String, ?dependencies:Array<String>):NgModule;
	public static function element(sel:String):Element;
}

extern class NgModule
{
	public function controller(name:String, def:Dynamic):NgModule;
	public function directive(name:String, def:Void->Dynamic):NgModule;
	public function factory(name:String, def:Dynamic):NgModule;
	
	public function config(cb:Dynamic):NgModule;
}

extern class NgRoute
{
	
}

extern class NgRouteProvider
{
	public function when(path:String, config:Dynamic):NgRouteProvider;
}

extern class NgSCE
{
	public function trustAs(type:String, value:Dynamic):Dynamic;
	public function trustAsHtml(value:Dynamic):Dynamic;
	public function trustAsUrl(value:Dynamic):Dynamic;
	public function trustAsResourceUrl(value:Dynamic):Dynamic;
	public function trustAsJs(value:Dynamic):Dynamic;
}

typedef DirDefinition = 
{
	restrict 		: String,
	?replace		: Bool,
	?template		: String,
	?templateUrl	: String,
	?scope			: Dynamic,
	?link			: Dynamic,
	?controller		: Array<Dynamic>,
	
}
