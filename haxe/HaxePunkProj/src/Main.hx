import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;

class Main extends Engine
{

	override public function init()
	{
#if debug
		HXP.console.enable();
#end
		HXP.scene = new MainScene();
	}

	public static function main() { new Main(); }
	
	public function new()
	{
		super(640, 480, 60, false, RenderMode.BUFFER);
	}

}