package graphics;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Canvas;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.masks.Grid;
import com.haxepunk.tmx.TmxLayer;
import com.haxepunk.tmx.TmxMap;
import com.haxepunk.tmx.TmxOrderedHash;
import com.haxepunk.tmx.TmxTileSet;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Rectangle;
import openfl.display.Graphics;


/**
 * A proper TMX implementation with multiple layers / tilesets support
 * @author Az
 */
class MyTmx extends Entity
{
	public static inline var ISOMETRIC 	= "isometric";
	public static inline var ORTHOGONAL	= "orthogonal";
	
	private var map 	: TmxMap; 
	private var gfx 	: TmxOrderedHash < TmxOrderedHash<Tilemap> > ;

	private var debug_collision : Graphic;
	
	private var is_initialized : Bool = false;
	
	private var _collision_layer : String;
	
	public function new(map_source:String) 
	{
		super();
		map 	= TmxMap.loadFromFile(map_source);
		type 	= "solid";		
	}
	
	public inline function tmx():TmxMap
	{
		return map;
	}
	
	/**
	 * Initialize graphics data
	 */
	private function init():Void
	{
		loadTileData();
		
		gfx		= new TmxOrderedHash();
		
		for (l in map.layers)
		{
			setupLayer(l);			
		}
		
		if (_collision_layer != null)
		{
			updateCollisionGrid();
		}
	}
	
	
	override public function added():Void 
	{
		if (!is_initialized)
		{
			init();
			is_initialized = true;
		}
		
		super.added();
	}
	
	public function setCollisionLayer(name:String):Void
	{
		if (map.layers.exists(name))
		{
			_collision_layer = name;
			
			if (is_initialized)
			{
				updateCollisionGrid();
			}
		}
		else
		{
			throw 'Layer "$name" does not exist!';
		}
	}
	
	private function setupLayer(l:TmxLayer):Void
	{
		var t 			: TmxTileSet;
		var tilemaps 	: TmxOrderedHash<Tilemap> = new TmxOrderedHash();
		
		for (t in map.tilesets)
		{
			var tmap	: Tilemap;
			
			if (map.orientation == ISOMETRIC)
			{
				tmap = new IsoTilemap(t.image, map.fullWidth, map.fullHeight, t.tileWidth, t.tileHeight, t.spacing, t.spacing);
			}
			else
			{
				tmap = new Tilemap(t.image, map.fullWidth, map.fullHeight, t.tileWidth, t.tileHeight, t.spacing, t.spacing);
			}
						
			tilemaps.set(t.name, tmap);
			addGraphic(tmap);
		}
		
		gfx.set(l.name, tilemaps );
		updateLayerRect(l.name);
	}
	
	
	private function updateCollisionGrid():Void
	{
		var grid : Grid;
		
		var layer : TmxLayer = map.getLayer(_collision_layer);
		
		if (mask == null)
		{
			if (map.orientation == ISOMETRIC)
			{
				mask = new IsoGrid(map.fullWidth, map.fullHeight, map.tileWidth, map.tileHeight);
			}
			else
			{
				mask = new Grid(map.fullWidth, map.fullHeight, map.tileWidth, map.tileHeight);
			}
		}
		
		grid = cast mask;
		
		// Loop through tile layer ids
		for (row in 0...layer.height)
		{
			for (col in 0...layer.width)
			{
				var gid = layer.tileGIDs[row][col];
				if (gid == 0) continue;
			
				grid.setTile(col, row, true);		
				
				// Edges
				if (col == 0 || row == 0)
					grid.setTile(col, row, true);
			}
		}

		setHitbox(grid.width, grid.height);
		
		var bmp = new BitmapData(map.fullWidth, map.fullHeight, true, 0x00000000);
		var img = new Image(bmp);
		
		var s : Sprite = new Sprite();
		
		grid.debugDraw(s.graphics, 1, 1);
		
		bmp.draw(s);
		img.updateBuffer();
		img.update();
		addGraphic(img);
		
		trace("Collision set!");
	}
	
	public function setTile(layer:String, x:Int, y:Int, gid:Int):Void
	{
		var t 	: TmxTileSet 	= getTileset(gid);
		var tid : Int 			= t.fromGid(gid);
		
		var tilemap : Tilemap 	= gfx.get(layer).get(t.name); 
		
		tilemap.setTile(x, y, tid);
		//trace('Setting tile: $x $y $gid of ${t.name}');
		
		if ( _collision_layer == layer)
		{
			// Set Collision
			var collision_flag : Bool = gid > 0 ? true : false;
			setTileCollision(x, y, collision_flag);
		}
	}
	
	private function setTileCollision(x:Int, y:Int, collision:Bool):Void
	{
		// Update collision
		if (is_initialized)
		{
			cast(mask, Grid).setTile(x,y, collision);
		}
	}
	
	public function unsetTile(layer:String, x:Int, y:Int):Void
	{
		if (!is_initialized)
			init();
		
		for (tilemap in gfx.get(layer))
		{
			tilemap.setTile(x, y , 0);
		}
		
		setTileCollision(x, y , false);
	}
	
	public function updateLayerRect(layer:String, ?rect:Rectangle):Void
	{
		var tmx_layer : TmxLayer = map.getLayer(layer);
		
		if (rect == null)
		{
			rect = new Rectangle(0, 0, tmx_layer.width, tmx_layer.height);
		}
		
		// Loop through tile layer ids
		for (row in 0...tmx_layer.height)
		{
			for (col in 0...tmx_layer.width)
			{
				var gid = tmx_layer.tileGIDs[row][col];
				
				if (gid != 0)
				{
					setTile(layer, col, row, gid);
				}
			}
		}
		
		
		update();	
	}
	
	/**
	 * Loads images for each tileset
	 */
	private function loadTileData():Void
	{
		for (t in map.tilesets)
		{
			t.set_image(Assets.getBitmapData('maps/' + t.imageSource));
			
			trace('Loaded tileset: ${t.name} - ${t.firstGID} - ${t.numTiles}');
		}		
	}
	
	private function getTileset(gid:Int):TmxTileSet
	{
		var t:TmxTileSet;
		
		for (t in map.tilesets)
		{
			if (gid >= t.firstGID && gid <= (t.numTiles + t.firstGID))
			{
				return t;
			}
		}
		
		throw 'Tileset not found for tile id: $gid';
	}
	
	//public function loadMask(collideLayer:String = "collide", typeName:String = "solid"):Void
	//{
		//if (!map.layers.exists(collideLayer))
		//{
			//#if debug
				//trace("Layer '" + collideLayer + "' doesn't exist");
			//#end
		//}
		//
		//var gid:Int;
		//var layer:TmxLayer = map.layers.get(collideLayer);
		//var grid = new Grid(map.fullWidth, map.fullHeight, map.tileWidth, map.tileHeight);
//
		//// Loop through tile layer ids
		//for (row in 0...layer.height)
		//{
			//for (col in 0...layer.width)
			//{
				//gid = layer.tileGIDs[row][col];
				//if (gid == 0) continue;
			//
				//grid.setTile(col, row, true);				
			//}
		//}
//
		////untyped console.log("Collision grid: ", grid);
		//this.mask = grid;
		//this.type = typeName;
		//setHitbox(grid.width, grid.height);
		//
		//var bmp = new BitmapData(map.fullWidth, map.fullHeight, true, 0x00000000);
		//var img = new Image(bmp);
		//
		//var s : Sprite = new Sprite();
		//
		//grid.debugDraw(s.graphics, 1, 1);
		//
		//bmp.draw(s);
		//img.updateBuffer();
		//img.update();
		//addGraphic(img);
		//
	//}
	
	/**
	 * Draws another tmx map over the current one (will draw only matching layers)
	 * @param	tmx
	 * @param	x
	 * @param	y
	 * 
	 * @return returns true if at least one tile has been drawn
	 */
	public function drawTMX(tmx:TmxMap, x:Int, y:Int):Bool
	{
		var l : TmxLayer;
		var t : TmxTileSet;
		
		var result = false;
		
		for (l in tmx.layers)
		{
			if (map.layers.exists(l.name))
			{
				var source : TmxLayer = l;
				var target : TmxLayer = map.getLayer(source.name);
				
				for (row in 0...source.height)
				{
					for (col in 0...source.width)
					{
						var gid 			: Int 			= source.tileGIDs[row][col];
						
						// Nothing to see here, carry on...
						if (gid == 0)
							continue;
						
						// Current tile is out of bounds	
						if (row > target.height || col > target.width)
							continue;
						
						var sourceTileset	: TmxTileSet 	= tmx.getGidOwner(gid); 
						var gid_offset		: Int			= sourceTileset.firstGID;
						
						var matchingTileset : TmxTileSet	= getMatchingTileset(sourceTileset);
						
						// Deterine tile GID offset value
						if (matchingTileset != null )
						{
							gid_offset = matchingTileset.firstGID - sourceTileset.firstGID;
							trace('Tileset found! ${matchingTileset.firstGID} - ${sourceTileset.firstGID} = $gid_offset');
						}
						else
						{
							importTileset(sourceTileset);
							gid_offset = sourceTileset.firstGID - gid_offset;
							trace('Tileset NOT found! ${sourceTileset.firstGID} - ?? = $gid_offset');
						}
						
						gid += gid_offset;
						
						target.tileGIDs[row][col] = gid;
						result = true;
						
						// Only set tiles if already initialized, else there is no point
						if (this.is_initialized)
						{
							setTile(target.name, col, row, gid);
						}	
					}
				}
			}
		}
		
		if (this.is_initialized)
		{
			update();
		}
		
		return result;
	}
	
	/**
	 * Check if current tmx contains tileset
	 * 
	 * @param	t
	 */
	private function getMatchingTileset(t:TmxTileSet):TmxTileSet
	{
		for (ts in map.tilesets)
		{
			if (ts.imageSource == t.imageSource)
			{
				return ts;
			}
		}
		
		return null;
	}
	
	/**
	 * Imports a tileset into the current map
	 * @param	t
	 */
	private function importTileset(t:TmxTileSet):TmxTileSet
	{
		var lastTS : TmxTileSet= null;
		
		// Seek the greatest GID index
		for (ts in map.tilesets)
		{
			if (lastTS == null || ts.firstGID > lastTS.firstGID)
			{
				lastTS = ts;
			}
		}
		
		// Create new index for new tileset
		t.firstGID = lastTS.firstGID + lastTS.numTiles;
		
		map.tilesets.push(t);
		
		return t;
	}
	
	
}