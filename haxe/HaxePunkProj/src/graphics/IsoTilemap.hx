package graphics;
import com.haxepunk.graphics.Tilemap;
import openfl.display.BitmapData;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Az
 */
class IsoTilemap extends Tilemap
{
	override public function draw(x:Float, y:Float, source:BitmapData, rect:Rectangle = null) 
	{
		var tile_half_width : Float = _tile.width /2;
		var tile_half_heigh : Float = _tile.height / 4;
		
		x = x / _tile.width;
		y = y / _tile.height;
		
		var isoX : Float = (x - y) * tile_half_width + (width / 2);
		var isoY : Float = (x + y) * tile_half_heigh;
		
		//trace("Iso Draw : ($x, $y) -> ($isoX, $isoY)");
		
		super.draw(isoX, isoY, source, rect);
	}
	
}