import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import com.haxepunk.graphics.Tilemap;
import com.haxepunk.Scene;
import com.haxepunk.tmx.TmxEntity;
import com.haxepunk.tmx.TmxMap;
import com.haxepunk.tmx.TmxTileSet;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.HXP;
import core.bhav.Wander;
import core.mob.NPC;
import core.mob.Player;
import graphics.MyTmx;
import openfl.geom.Point;
import core.mob.Mob;
import openfl.Assets;

class MainScene extends Scene
{
	private var player 	: Entity;
	private var map 	: MyTmx;
	
	public override function begin()
	{
		var text : Text = new Text('Hello Nurse !!!');
		player = new Player();
		player.x = 32 * 2;
		player.y = 32 * 4;
		
		addGraphic(text);
		
		createMap();
		add(player);
		//createMobs();
		
		
		
		HXP.console.enable();
		HXP.console.debug = true;
		HXP.console.debugDraw = true;
	}
	
	private function createMap():Void
	{
		var e : MyTmx = new MyTmx("maps/isometric_grass_and_water.tmx");
		//e.drawTMX(TmxMap.loadFromFile('maps/obj.tmx'), 10, 25);
		e.setCollisionLayer("Top");
		
		add(e);
		e.unsetTile("Ground", 0, 0);
		
		map = e;
		
		
	}
	
	private function createMobs()
	{
		for (i in 0...50)
		{
			var mob = new NPC();
			mob.x = 32 * Std.random(100);
			mob.y = 32 * Std.random(100);
			mob.addBehavior(new Wander(map.tmx()));
			
			//trace('Create mob: ${mob.x}, ${mob.y}, ${map.width}');
		
			add(mob);
		}
	}
	
	override public function update() 
	{
		
		var speed 	: Int 	= 2;
		var camera 	: Point = HXP.camera;
		
		var x = player.x;
		var y = player.y;
		
		
		try
		{
			// Run
			if (Input.check(Key.SHIFT))
			{
				speed *= 2;
			}
			
			if (Input.check(Key.UP))
			{
				player.y -= speed;
				//map.moveBy(0, -speed);
			}
			
			if (Input.check(Key.DOWN))
			{
				player.y += speed;
			}
			
			if (Input.check(Key.LEFT))
			{
				player.x -= speed;
			}
			
			if (Input.check(Key.RIGHT))
			{
				//map.moveBy(speed, 0);
				player.x +=  speed;				
			}
			
			
			if (Input.check(Key.ANY))
			{
				//trace('Map pos: ${map.x}x${map.y}');
			}
			
			// Check collision
			
			if (player.collide("solid", player.x, player.y) != null)
			{
				// Revert position
				player.x = x;
				player.y = y;
				
				trace('colide!');
			}
			
			
			
			camera.x = player.x - HXP.halfWidth;
			camera.y = player.y - HXP.halfHeight;
			
		}
		catch (e:Dynamic)
		{
			trace("Error coght!");
		}
		
		super.update();
		
		
	}
}