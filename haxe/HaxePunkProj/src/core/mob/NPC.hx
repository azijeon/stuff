package core.mob;
import core.bhav.Wander;
import com.haxepunk.graphics.Image;

/**
 * ...
 * @author Az
 */
class NPC extends Mob
{

	public function new() 
	{
		super();
		this.setHitbox(30, 32, -2, -32);
		
		addGraphic(new Image('graphics/sarah.png'));		
	}
	
}