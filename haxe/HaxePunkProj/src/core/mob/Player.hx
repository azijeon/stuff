package core.mob;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.Mask;
import openfl.display.BitmapData;
import openfl.display.Sprite;

/**
 * ...
 * @author Az
 */
class Player extends Mob
{
	public function new() 
	{
		super();		
		name = "Player";
		
		this.setHitbox(30, 32, -2, -32);
		this.type = "solid";
		
		addGraphic(new Image('graphics/sarah.png'));		
	}
	
	
}