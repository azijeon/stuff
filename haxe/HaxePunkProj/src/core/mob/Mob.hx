package core.mob;

import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.Mask;
import core.bhav.BhavQueue;
import core.bhav.IBehavior;
import core.GameObject;

/**
 * ...
 * @author Az
 */
class Mob extends GameObject
{
	private var zIndex 	: Int;
	
	public function new() 
	{
		super();
		this.type = "solid";		
	}
	
	override public function added():Void 
	{
		super.added();
		zIndex = layer;	
	}
	
	override public function update():Void 
	{
		var e : Entity = collide("solid", x, y);
		
		if (e != null)
		{
			trace('Player collides with ${e.name}');
		}
		
		layer = cast zIndex - (y + this.halfHeight); 
		
		super.update();
	}
	
	
	
	
}