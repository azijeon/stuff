package core.bhav;
import com.haxepunk.tmx.TmxMap;
import com.haxepunk.Tween;
import com.haxepunk.tweens.motion.LinearMotion;
import com.haxepunk.tweens.motion.LinearPath;
import core.GameObject;

/**
 * ...
 * @author Az
 */
class Wander implements IBehavior
{
	var map : TmxMap;
	
	public function new(map:TmxMap) 
	{
		this.map = map;
	}
	
	public function checkPrerequisites(actor:GameObject):Bool
	{
		return true;
	}
	
	public function isRunning(actor:GameObject):Bool
	{
		return actor.exists("wander_to");
		
		//if (actor.exists("wander_to"))
		//{
			//var t : Tween = cast actor.get("wander_to");
			//
			//return t.percent < 1;
		//}
		
		return false;
	}
	
	public function isComplete(actor:GameObject):Bool
	{
		var t : Tween = cast actor.get("wander_to");
		
		if (t != null && t.percent == 1)
			return true;
		
		return false;
	}
	
	public function execute(actor:GameObject):Void
	{
		if (!isRunning(actor))
		{
			init(actor);			
		}
		else
		{
			
			var t : LinearMotion = tween(actor);
			
			if (actor.collide("solid", t.x, t.y) == null)
			{
				actor.x = t.x;
				actor.y = t.y;
			}
			else
			{
				terminate(actor);
			}			
		}
	}
	
	private function init(actor:GameObject):Void
	{
		var maxX = map.fullWidth;
		var maxY = map.fullHeight;
		
		var x = Std.random(maxX) - actor.x;
		var y = Std.random(maxY) - actor.y;
		
		var t : LinearMotion = new LinearMotion();
		t.x = actor.x;
		t.y = actor.y;

		actor.addTween(t, true);
		
		t.setMotionSpeed(actor.x, actor.y, x, y, 15);
		t.start();
		
		actor.set("wander_to", t);
	}
	
	private inline function tween(actor:GameObject):LinearMotion
	{
		return cast actor.get("wander_to");
	}
	
	public function terminate(actor:GameObject):Void
	{
		var t = tween(actor);
		
		actor.removeTween(t);
		actor.remove('wander_to');
		
		actor.addBehavior(this);
	}
	
}