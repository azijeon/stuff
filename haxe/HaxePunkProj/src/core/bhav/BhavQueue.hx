package core.bhav;
import core.GameObject;

/**
 * ...
 * @author Az
 */
class BhavQueue
{
	private var queue : Array<IBehavior>;
	
	public function new() 
	{
		queue = [];
	}
	
	public function current():IBehavior
	{
		var index : Int = queue.length - 1;
		
		if (index >= 0)
		{
			return queue[index];
		}
		return null;
	}
	
	public function push(b:IBehavior):Void
	{
		queue.push(b);
	}
	
	public function execute(actor:GameObject):Void
	{
		if (queue.length > 0)
		{
			var bhav : IBehavior = current();
			
			bhav.execute(actor);
			
			if (bhav.isComplete(actor))
			{
				bhav.terminate(actor);
				queue.pop();				
			}
		}		
	}
	
	public inline function length() : Int
	{
		return queue.length;
	}
	
}