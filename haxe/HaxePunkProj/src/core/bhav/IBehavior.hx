package core.bhav;
import com.haxepunk.Entity;
import core.GameObject;
import haxe.ds.StringMap;

/**
 * ...
 * @author Az
 */
interface IBehavior
{
	
	//public function effects():StringMap<Int>;
	//public function checkPrerequisites(actor:GameObject):Bool;
	
	public function isComplete(actor:GameObject):Bool;
	public function execute(actor:GameObject):Void;	
	
	public function terminate(actor:GameObject):Void;
}