package core;
import com.haxepunk.Entity;
import core.bhav.BhavQueue;
import core.bhav.IBehavior;
import haxe.ds.StringMap;

/**
 * ...
 * @author Az
 */
class GameObject extends Entity
{

	private var data	: StringMap<Dynamic>;
	private var queue 	: BhavQueue;
		
	public function new() 
	{
		super();
		queue 	= new BhavQueue();
		data 	= new StringMap();
	}
	
	public function addBehavior(b:IBehavior):Void
	{
		queue.push(b);
	}
	
	override public function update():Void 
	{
		queue.execute(this);
		super.update();
	}
	
	public function set(name:String, value:Dynamic):Void
	{
		data.set(name, value);
	}
	
	public function get(name:String):Dynamic
	{
		return data.get(name);
	}
	
	public function remove(name:String):Void
	{
		data.remove(name);
	}
	
	public function exists(name:String):Bool
	{
		return data.exists(name);
	}
	
}