<?php
namespace Server;

/**
 * Description of ClientConnection
 *
 * @author Nikolay Dachin
 */
class ClientConnection
{
	protected $sock;
	protected $buffer = '';
	
	public function __construct($sock)
	{
		if(is_resource($sock))
		{
			$this->id	= (int)$sock;
			$this->sock = $sock;
		}
	}
	
	public function id()
	{
		return $this->id;
	}
		
	protected function getHeader()
	{
		$header		= [];
		$header[]	= "HTTP/1.1 101 Switching Protocols\r\n";
		
	}


	/**
	 * Write to client
	 * 
	 * @param type $message
	 */
	public function write($message)
	{
		$length = strlen($message);
		
		while(true)
		{
			$sent	= \socket_write($this->sock, $message, $length);
			
			
			if($sent === false || !is_numeric($sent))
			{
				$this->error("Write failed!", socket_last_error());
				break;
			}
			
			if($sent < $length)
			{
				// If the whole message is not sent, recalculate next part
				$message = substr($message, $sent);
				$length -= $sent;
			}
			else
			{
				// All is well
				break;
			}			
		}		
	}
	
	public function clear()
	{
		$this->buffer = '';
	}
	
	/**
	 * Read from client
	 */
	public function read($length = 1024, $type = PHP_NORMAL_READ)
	{
		$this->buffer .= \socket_read($this->sock, $length, $type); 
		return $this->buffer;
	}
	
	public function readWhile($terminate, $length = 1024, $type = PHP_NORMAL_READ)
	{
		
	}
	
	public function recv($length = 1024, $flags = 2)
	{
		$count	= 0;
		$buf	= [];
		$out	= [];
		
		while(\socket_recv($this->sock, $buf, $length, MSG_PEEK) > 0)
		{
			var_dump($buf);
			
			if(++$count > 10)
			{
				die("WTF");		
			}
			break;
		}	
		
		return $buf;
	}
	
	protected function error($message, $code = null)
	{
		$error = $message;
		
		if(!is_null($code))
		{
			 $error .=  ' : ' . socket_strerror($code);
		}
		
		var_dump($error);		
	}
	
	public function onUserInput()
	{
		
	}
	
	public function sock()
	{
		return $this->sock;
	}
	
	public function close()
	{
		\socket_close($this->sock);
	}
}
