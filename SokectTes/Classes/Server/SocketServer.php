<?php
namespace Server;

/**
 * Description of Server
 *
 * @author Nikolay Dachin 
 */
class SocketServer
{
	protected $sock;
	protected $max_conn = 2;
	
	protected $time_started;
	
	protected $is_running = false;
	protected $clients;


	public function __construct()
	{
		$this->clients = []; //new \SplObjectStorage();
	}
	
	public function start($port)
	{
		if($this->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))
		{
			$this->log('Socket created...');			
		}
	
		
		if((\socket_bind($this->sock, 'localhost', $port)) === false)
		{
			$this->error('Failed to bind socket!');			
		}
		else
		{
			$this->log('Socket bound...');
	
		}
		
		if(\socket_listen($this->sock, $this->max_conn) === false)
		{
			$this->error('Listen failed');
		}
		
		$this->time_started = time();
		$this->is_running	= true;
		
		$this->log('Server started ...');
		
		$this->run();
	}
	
	public function stop()
	{
		if($this->is_running)
		{
			$this->is_running = false;
			@\socket_close($this->sock);
			$this->log('Server stopped ...');
		}
	}
	
	protected function run()
	{
		try
		{
		
			while($this->is_running)
			{
				// Setup read list
				$read	= array();
				$write	= array();
				$exept	= array();
				$timeout= null;

				$read[] = $this->sock;

				foreach($this->clients as $c)
				{
					$read[] = $c->sock();				
				}

				// Select blocking select
				$ready_count = (int)\socket_select($read, $write,$exept, $timeout);

				if($ready_count < 1)
					continue;

				
				foreach($read as $sock)
				{
					// Accept new connections (if $this->sock remains in $read after socket select, than someone is trying to connect to the server)
					if($this->sock === $sock)
					{
						$this->acceptConnection();
						continue;
					}	
					
					// Handle client 
					$client = $this->getClient($sock);
					$this->processClient($client);		
					
				}
				
				
					
			}
		}
		catch(\Exception $e)
		{
			$this->error($e->getMessage());
		}
		
	}
	
	/**
	 * Accept an incomming client connection and add client to client pool
	 * 
	 * @throws Exception
	 */
	protected function acceptConnection()
	{
		if(count($this->clients) < $this->max_conn)
		{
			$client = \socket_accept($this->sock);
			$this->addClient(new ClientConnection($client));

			$this->writeAll("A client has joined!\n\r");
		}
		else
		{
			throw new Exception('Maximum number of clients reached!');
		}
	}


	public function write($message)
	{
		if(\socket_write($this->sock, $message, count($message)) === false)
		{
			$this->error("Write failed!", socket_last_error());
		}
	}
	
	/**
	 * Write to all clients
	 * 
	 * @param type $message
	 */
	protected function writeAll($message)
	{
		foreach($this->clients as $c)
		{
			$c->write($message);
		}
	}
	
	protected function error($message)
	{
		$error	= $message;
		$code	= socket_last_error($this->sock); 
		
		if($code)
		{
			$error .=  ' : ' . socket_strerror($code);
		}
		
		var_dump($error);		
		
		$this->stop();
		
		// It is currently mandatory to die on error, as the loop goes on into run otherwhise
		exit;
	}
	
	/**
	 * @param int|socket $id
	 * 
	 * @return ClientConnection 
	 */
	public function getClient($id)
	{
		return $this->clients[(int)$id];
	}
	
	public function addClient(ClientConnection $c)
	{
		$this->clients[$c->id()] = $c;
		
		$count = count($this->clients) + 1;
		$c->write("Hello, Nurse {$count} !!!\n");
	}
	
	public function removeClient(ClientConnection $c)
	{
		unset($this->clients[$c->id()]);
		$c->close();
		
		$this->writeAll("Client {$c->id()} has disconnected!\r\n");
	}
	
	protected function log($message)
	{
		echo date('Y-M-D H:i:s : ') , $message, '<br/>';
		flush();
		ob_flush();
		
	}	
	
	protected function processClient(ClientConnection $client)
	{
		$count	= 0;
		$buf	= [];
		
		$input = $client->read();
		
		if($input == null)
		{
			// Client disconnected
			$this->removeClient($client);
			return;
		}

		if(!preg_match('~\r?\n\r?\n$~', $input))
				return;
		
		$this->log('Input: ' . $input);
		
		$input = trim($input);
		
		if($input == "exit")
		{
			// Client requested disconnect
			$this->removeClient($client);
			return;
		}

		if($input == "stop")
		{
			$this->stop();
			return;
		}

		
		$this->log('Input: ' . $input);
			
		foreach($this->clients as $c)
		{
			if($c->id() == $client->id())
			{
				$client->write("You said: {$input}\n\r");
			}
			else
			{
				$c->write("User {$client->id()} said: {$input}\n\r");
			}
			
			$client->clear();
		}
		
		
		
	}
	
	public function __destruct()
	{
		$this->stop();
	}
}
