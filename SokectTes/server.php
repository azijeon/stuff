<?php
include 'autoload.php';

header( 'Content-type: text/html; charset=utf-8' );

set_time_limit(0);

$port = isset($_GET['port']) ? (int)$_GET['port'] : 823;

$server = new Server\SocketServer();
$server->start($port);
