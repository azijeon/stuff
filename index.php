<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Stuff</title>
    </head>
    <body>
		<?php

		$files = array_diff(\scandir(__DIR__), ['..', '.', '.git', 'nbproject', 'index.php']);
		
		?>
		
		<ol>
		<?php foreach($files as $file): ?>
			<li>
				<a href="<?= $file ?>"><?= $file; ?></a>
			</li>		
		<?php endforeach; ?>
		</ol>
    </body>
</html>
