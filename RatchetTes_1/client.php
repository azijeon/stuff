<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ratchet WebSocket Tes</title>
    </head>
    <body>
		<pre id="MessageArea">
			
		</pre>
		
		<textarea id="Input" type="text"></textarea>
		<button onclick="send()">Send</button>
		<button onclick="closeSocket()">Close</button>
		
		<script type="text/javascript">
			
			var port = <?= isset($_GET['port']) ? (int)$_GET['port'] : 823; ?>
			
			var sock = new WebSocket("ws://localhost:" + port);
			
			sock.onopen = function(e)
			{
				document.getElementById("MessageArea").innerHTML += "Connected...\n";
				sock.send("I am connected!");
			}
			
			sock.onmessage = function(e)
			{
				document.getElementById("MessageArea").innerHTML += e.data + "\n";
			}
			
			
			function send()
			{
				var input = document.getElementById("Input");
				
				console.log(input);
				
				sock.send(input.value);
				input.value = '';
			}
			
			function closeSocket()
			{
				console.log("Closing");
				sock.send("I'm going off");
				sock.close();
				
				document.getElementById("MessageArea").innerHTML = "Closed";
			}
			
						
		</script>
		
		<?php
		
		// put your code here
		?>
    </body>
</html>
